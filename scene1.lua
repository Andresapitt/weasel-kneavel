---------------------------------------------------------------------------------
--
-- scene.lua
--
---------------------------------------------------------------------------------

local sceneName = ...

local sheetInfo = require("weasel")
local composer = require( "composer" )

-- Load scene with same root filename as this file
local scene = composer.newScene( sceneName )

composer.recycleOnSceneChange = true

local physics = require( "physics" )
physics.start()

physics.setGravity( 0, 9.8 )

local _W = display.contentWidth
local _H = display.contentHeight
local xMin = display.screenOriginX
local yMin = display.screenOriginY
local xMax = display.contentWidth - xMin
local yMax = display.contentHeight - yMin
local xWidth = xMax-xMin;
local yHeight = yMax-yMin
local padding = 10

--Booleans
--Currently gameIsActive isn't used. But if you added a pause button you can set this to false and the gameLoop would stop running.
local gameIsActive = true 
local inAir = false
local tapped = false
local trans1
local stopBarAnimation = false;

--Maths vars localised for speed here
local mRound = math.round 
local mRand = math.random 

--Game Variables
local grav = 1
local frameNum = 0
local yVelocity, xVelocity, power
local maxHeight, distance = 0, 0


---------------------------------------------------------------------------------

function scene:create( event )
    local sceneGroup = self.view
    
--     --------------------------------------------------------------------------------
--     -- Build Camera
--     --------------------------------------------------------------------------------
    local perspective = require("perspective")  -- Code Exchange library to manage camera view
    local camera = perspective.createView(5)

    local weaselY=472
    
--     --------------------------------------------------------------------------------
     local background = display.newImageRect("images/BackGround1.png", 2048, 640)
     background.anchorX=0
    background.x =0; background.y = _H*.5

    local foreground = display.newImageRect("images/ForeGround.png", 2048, 116)
     foreground.anchorX=0
    foreground.x = _0; foreground.y = _H - foreground.height*.5
    physics.addBody( foreground, "static", { density=2, friction=.2, bounce=0 } )


    local instructText = display.newImageRect("images/tap1.png", 256, 48)
    instructText.x = _W*.5; instructText.y = yMax-20
    
    local target = display.newImageRect("images/target.png", 114,389)
    ---target.anchorY=0
    target.x = 1580; target.y = 330

    local woodLarge = display.newImageRect("images/WoodPlankLarge.png",34,300)
    woodLarge.x = 1170; woodLarge.y = 330
     physics.addBody( woodLarge, { density=3.2, friction=0.5, bounce=0.1 } )

    --Power bar, and scaling rect
    local bar = display.newRect( 0,0,1,26)
    --bar:setReferencePoint(display.TopLeftReferencePoint)
    bar.anchorX = 0
    bar.anchorY = 0
    bar.x = xMin+padding; bar.y = yMin+padding; bar:setFillColor(1,138/255,0)


    local rectBar = display.newImageRect("images/bar.png", 122,28)
    --rect:setReferencePoint(display.TopLeftReferencePoint)
    rectBar.anchorX = 0
    rectBar.anchorY = 0
    rectBar.x=xMin+padding; rectBar.y=yMin+padding



    
    
    local scaleFactor = 1.0
    local physicsData = (require "weasel_physics").physicsData(scaleFactor)

    local myImageSheet = graphics.newImageSheet( "images/weasel.png", sheetInfo:getSheet() )
    local plasma = display.newSprite( myImageSheet , sheetInfo:getSequenceData() )
    plasma.width = 155
    plasma.height = 104
    plasma.x = 142
    plasma.y = weaselY
    plasma.rotation=0
    plasma.isFixedRotation=true
    physics.addBody( plasma, physicsData:get("Weasel") )
   -- physics.addBody( plasma, { density=3, friction=1.0, bounce=0 } )
    plasma:setSequence( "blink" )
    plasma:play()


    --Quick Function for scaling the power-bar
    local function scale()
        local function reset()
            bar.xScale = 1
            if not stopBarAnimation then
            scale();
            end
        end
        --if tapped == true then
        print("scaling")
        trans1 = transition.to( bar, {time= 750, xScale=124, onComplete=reset})
        --end
    end

    --Tap listener, this essentially controls the power of our launch!
    function tap (event)
        if tapped == true then
            --Stop the transition.
            stopBarAnimation = true;
            if trans1 then transition.cancel(trans1); trans1 = nil; end

            --Set our various gameplay variables.
            power = mRound(bar.xScale)
           -- yVelocity = power/2
            xVelocity = power *100 --Change 3 to change speed of game! This is the original yVelocity on launch
            inAir = true

            --Now change our tap/touch listeners and then the text.
            Runtime:removeEventListener("tap", tap)
          --  Runtime:addEventListener("touch", jump)
          --print("vel= "..xVelocity)
          plasma:applyForce( xVelocity, 900, plasma.x, plasma.y) 
          plasma:setSequence( "driving" )
          plasma:play()

           camera:add(plasma, 1)
           camera.damping = 10 -- A bit more fluid tracking


            -- Only start the camera tracking when the penguin has moved pasted the center of the view port
                local function trackPlasma( event )
                    
                end

                Runtime:addEventListener( 'enterFrame', gameLoop )
           
            --Instructtext.
          display.remove(instructText)
         --   instructText = display.newImageRect(uiGroup, "images/tap3.png", 256, 48)
         --   instructText.x = _W*.5; instructText.y = yMax-20
            --audio.play(boostSound) --Play the sound..
        else
            scale()
            tapped = true

        --Instructtext.
            display.remove(instructText)
            instructText = display.newImageRect("images/tap2.png", 256, 48)
            instructText.x = _W*.5; instructText.y = yMax-20
        end
        return true 
    end --end tap

    local function gameOver()
         print("game over")
        power=0
        tapped=true
        transition.cancel(trans1); 
        trans1 = nil; 
        camera:destroy()   
        composer.gotoScene( "gameOver" )
    end
    
    local function checkWinorLoose()
        local sequence = "loose"
        if plasma.x > target.x and   plasma.x < (target.x+target.width)  then
            sequence = "win"
        end
        print("result:"..sequence)
        return sequence
    end
    

        --The gameLoop controls moving all the items in the game as well as controlling the spawning of items!    
    function gameLoop(event)

        if plasma.x > 600 then
            camera:setFocus(plasma) -- Set the focus to the weasel so it tracks it
        end

        local vx, vy = plasma:getLinearVelocity()
        --print("xVelocity="..vx)
        if vx == 0 or plasma.x > 2000 then
            local sequence = checkWinorLoose()
            plasma:setSequence(sequence)
            plasma:play()
            Runtime:removeEventListener( 'enterFrame', gameLoop )  
            print("before timer ")
            timer.performWithDelay(2000,gameOver,1)
           -- local currScene = composer.getSceneName( "current" )
          --  composer.gotoScene( currScene )
        end


    end --end gameLoop

  
    sceneGroup:insert(bar)
    sceneGroup:insert(rectBar)
    sceneGroup:insert(background)
    sceneGroup:insert(foreground)
    sceneGroup:insert(target)
    sceneGroup:insert(woodLarge)
    sceneGroup:insert(plasma)
   
    sceneGroup:insert(instructText)


   --local background = self:getObjectByTag( "background" )
   --local gameGroup = self:getObjectByTag( "gameGroup" )
    --local plasma = self:getObjectByTag( "Object21" )


    

    --local ufo = self:getObjectByTag( "ufo" )

    --background:addEventListener( "touch", shootPlasma )
    Runtime:addEventListener("tap", tap)
    
    

    ------------------------------------------------------------
    -- Camera code

    camera:setParallax(1,.6)
    camera:add( plasma, 1 )
    camera:add(rectBar,2)
    camera:add(bar,3)
    camera:add( target, 3 ) 
    camera:add(woodLarge,3)
    camera:add( foreground, 3 ) 
    camera:add( background, 4 ) 
    
    
    camera:setBounds( 0, 1400, 320, 320 ) -- used to keep our view from going outside the background
    -- print (camera:layerCount())
    ------------------------------------------------------------
end

function scene:show( event )
    local sceneGroup = self.view
    local phase = event.phase

    if phase == "will" then
        -- Called when the scene is still off screen and is about to move on screen

    elseif phase == "did" then
        -- Called when the scene is now on screen
        -- 
        -- INSERT code here to make the scene come alive
        -- e.g. start timers, begin animation, play audio, etc
        
        -- we obtain the object by id from the scene's object hierarchy

        
    end 
end

function scene:hide( event )
    local sceneGroup = self.view
    local phase = event.phase

    if event.phase == "will" then
        -- Called when the scene is on screen and is about to move off screen
        --
        -- INSERT code here to pause the scene
        -- e.g. stop timers, stop animation, unload sounds, etc.)
    elseif phase == "did" then
        -- Called when the scene is now off screen

    end 
end


function scene:destroy( event )
    local sceneGroup = self.view



    -- Called prior to the removal of scene's "view" (sceneGroup)
    -- 
    -- INSERT code here to cleanup the scene
    -- e.g. remove display objects, remove touch listeners, save state, etc
end

---------------------------------------------------------------------------------

-- Listener setup
scene:addEventListener( "create", scene )
scene:addEventListener( "show", scene )
scene:addEventListener( "hide", scene )
scene:addEventListener( "destroy", scene )

---------------------------------------------------------------------------------

return scene
