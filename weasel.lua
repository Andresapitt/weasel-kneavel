--
-- created with TexturePacker (http://www.codeandweb.com/texturepacker)
--
-- $TexturePacker:SmartUpdate:deae24a8f8674432fa2b87b30b740c0c:fa7678e909602429fe4851137a33de34:335e5fe926f8a095c0ee211febf64813$
--
-- local sheetInfo = require("mysheet")
-- local myImageSheet = graphics.newImageSheet( "mysheet.png", sheetInfo:getSheet() )
-- local sprite = display.newSprite( myImageSheet , {frames={sheetInfo:getFrameIndex("sprite")}} )
--

local SheetInfo = {}

SheetInfo.sheet =
{
    frames = {
    
        {
            -- Weasel_K_blink_rusty0001
            x=161,
            y=2,
            width=157,
            height=99,

            sourceX = 2,
            sourceY = 5,
            sourceWidth = 160,
            sourceHeight = 104
        },
        {
            -- Weasel_K_blink_rusty0002
            x=320,
            y=2,
            width=157,
            height=99,

            sourceX = 2,
            sourceY = 5,
            sourceWidth = 160,
            sourceHeight = 104
        },
        {
            -- Weasel_K_blink_rusty0003
            x=161,
            y=2,
            width=157,
            height=99,

            sourceX = 2,
            sourceY = 5,
            sourceWidth = 160,
            sourceHeight = 104
        },
        {
            -- Weasel_K_drive_rusty0001
            x=161,
            y=2,
            width=157,
            height=99,

            sourceX = 2,
            sourceY = 5,
            sourceWidth = 160,
            sourceHeight = 104
        },
        {
            -- Weasel_K_drive_rusty0002
            x=2,
            y=2,
            width=157,
            height=100,

            sourceX = 2,
            sourceY = 4,
            sourceWidth = 160,
            sourceHeight = 104
        },
        {
            -- Weasel_K_drive_rusty0003
            x=161,
            y=2,
            width=157,
            height=99,

            sourceX = 2,
            sourceY = 5,
            sourceWidth = 160,
            sourceHeight = 104
        },
        {
            -- Weasel_K_drive_rusty0004
            x=2,
            y=2,
            width=157,
            height=100,

            sourceX = 2,
            sourceY = 4,
            sourceWidth = 160,
            sourceHeight = 104
        },
        {
            -- Weasel_K_loose_rusty0001
            x=479,
            y=2,
            width=157,
            height=99,

            sourceX = 2,
            sourceY = 5,
            sourceWidth = 160,
            sourceHeight = 104
        },
        {
            -- Weasel_K_loose_rusty0002
            x=1274,
            y=2,
            width=157,
            height=97,

            sourceX = 2,
            sourceY = 7,
            sourceWidth = 160,
            sourceHeight = 104
        },
        {
            -- Weasel_K_loose_rusty0003
            x=1274,
            y=2,
            width=157,
            height=97,

            sourceX = 2,
            sourceY = 7,
            sourceWidth = 160,
            sourceHeight = 104
        },
        {
            -- Weasel_K_loose_rusty0004
            x=1274,
            y=2,
            width=157,
            height=97,

            sourceX = 2,
            sourceY = 7,
            sourceWidth = 160,
            sourceHeight = 104
        },
        {
            -- Weasel_K_loose_rusty0005
            x=1433,
            y=2,
            width=157,
            height=97,

            sourceX = 2,
            sourceY = 7,
            sourceWidth = 160,
            sourceHeight = 104
        },
        {
            -- Weasel_K_loose_rusty0006
            x=1274,
            y=2,
            width=157,
            height=97,

            sourceX = 2,
            sourceY = 7,
            sourceWidth = 160,
            sourceHeight = 104
        },
        {
            -- Weasel_K_loose_rusty0007
            x=1433,
            y=2,
            width=157,
            height=97,

            sourceX = 2,
            sourceY = 7,
            sourceWidth = 160,
            sourceHeight = 104
        },
        {
            -- Weasel_K_loose_rusty0008
            x=1274,
            y=2,
            width=157,
            height=97,

            sourceX = 2,
            sourceY = 7,
            sourceWidth = 160,
            sourceHeight = 104
        },
        {
            -- Weasel_K_loose_rusty0009
            x=1433,
            y=2,
            width=157,
            height=97,

            sourceX = 2,
            sourceY = 7,
            sourceWidth = 160,
            sourceHeight = 104
        },
        {
            -- Weasel_K_win_rusty0001
            x=638,
            y=2,
            width=157,
            height=99,

            sourceX = 2,
            sourceY = 5,
            sourceWidth = 160,
            sourceHeight = 104
        },
        {
            -- Weasel_K_win_rusty0002
            x=797,
            y=2,
            width=157,
            height=99,

            sourceX = 2,
            sourceY = 5,
            sourceWidth = 160,
            sourceHeight = 104
        },
        {
            -- Weasel_K_win_rusty0003
            x=956,
            y=2,
            width=157,
            height=99,

            sourceX = 2,
            sourceY = 5,
            sourceWidth = 160,
            sourceHeight = 104
        },
        {
            -- Weasel_K_win_rusty0004
            x=1115,
            y=2,
            width=157,
            height=99,

            sourceX = 2,
            sourceY = 5,
            sourceWidth = 160,
            sourceHeight = 104
        },
    },
    
    sheetContentWidth = 1592,
    sheetContentHeight = 104
}

SheetInfo.frameIndex =
{

    ["Weasel_K_blink_rusty0001"] = 1,
    ["Weasel_K_blink_rusty0002"] = 2,
    ["Weasel_K_blink_rusty0003"] = 3,
    ["Weasel_K_drive_rusty0001"] = 4,
    ["Weasel_K_drive_rusty0002"] = 5,
    ["Weasel_K_drive_rusty0003"] = 6,
    ["Weasel_K_drive_rusty0004"] = 7,
    ["Weasel_K_loose_rusty0001"] = 8,
    ["Weasel_K_loose_rusty0002"] = 9,
    ["Weasel_K_loose_rusty0003"] = 10,
    ["Weasel_K_loose_rusty0004"] = 11,
    ["Weasel_K_loose_rusty0005"] = 12,
    ["Weasel_K_loose_rusty0006"] = 13,
    ["Weasel_K_loose_rusty0007"] = 14,
    ["Weasel_K_loose_rusty0008"] = 15,
    ["Weasel_K_loose_rusty0009"] = 16,
    ["Weasel_K_win_rusty0001"] = 17,
    ["Weasel_K_win_rusty0002"] = 18,
    ["Weasel_K_win_rusty0003"] = 19,
    ["Weasel_K_win_rusty0004"] = 20,
}

SheetInfo.sequenceData = {
    {
    name="driving",
    frames={4,5,6,7},
    time=500,
    loopCount = 999,
    loopDirection = "forward"
    },
    {
    name="blink",
    frames={1,2,3},
    time=2000,
    loopCount = 100,
    },
    {
    name="loose",
    frames={8,9,10,11,12,13,14,15,16},
    time=1500,
    loopCount = 1,
    },
    {
    name="win",
    frames={17,18,19,20},
    time=500,
    loopCount = 1,
    },


}

function SheetInfo:getSheet()
    return self.sheet;
end

function SheetInfo:getSequenceData()
    return self.sequenceData;
end

function SheetInfo:getFrameIndex(name)
    return self.frameIndex[name];
end

return SheetInfo
