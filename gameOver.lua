---------------------------------------------------------------------------------
--
-- scene.lua
--
---------------------------------------------------------------------------------

local sceneName = ...

local composer = require( "composer" )

-- Load scene with same root filename as this file
local scene = composer.newScene( sceneName )

local _W = display.contentWidth
local _H = display.contentHeight
local xMin = display.screenOriginX
local yMin = display.screenOriginY
local xMax = display.contentWidth - xMin
local yMax = display.contentHeight - yMin
local xWidth = xMax-xMin;
local yHeight = yMax-yMin
local padding = 10




---------------------------------------------------------------------------------

function scene:create( event )
    local sceneGroup = self.view
    
--     --------------------------------------------------------------------------------
--     -- Build Camera
--     --------------------------------------------------------------------------------
   -- local perspective = require("perspective")  -- Code Exchange library to manage camera view
   -- local camera = perspective.createView(3)
    
--     --------------------------------------------------------------------------------
    local instructText = display.newImageRect("images/gameOver.png", _W, _H)
    instructText.x = _W*.5; instructText.y = _H*.5
    sceneGroup:insert(instructText)


     local function restart()
        print("closing gameOver")
        composer.gotoScene( "scene1" )
    end

    instructText:addEventListener("tap", restart)

   
    

    --local background = self:getObjectByTag( "background" )
    --local gameGroup = self:getObjectByTag( "gameGroup" )
    
end

function scene:show( event )
    local sceneGroup = self.view
    local phase = event.phase

    if phase == "will" then
        -- Called when the scene is still off screen and is about to move on screen

    elseif phase == "did" then
        -- Called when the scene is now on screen
        -- 
        -- INSERT code here to make the scene come alive
        -- e.g. start timers, begin animation, play audio, etc
        
        -- we obtain the object by id from the scene's object hierarchy

        
    end 
end

function scene:hide( event )
    local sceneGroup = self.view
    local phase = event.phase

    if event.phase == "will" then
        -- Called when the scene is on screen and is about to move off screen
        --
        -- INSERT code here to pause the scene
        -- e.g. stop timers, stop animation, unload sounds, etc.)
    elseif phase == "did" then
        -- Called when the scene is now off screen
        composer.removeScene( true )

    end 
end


function scene:destroy( event )
    local sceneGroup = self.view

    -- Called prior to the removal of scene's "view" (sceneGroup)
    -- 
    -- INSERT code here to cleanup the scene
    -- e.g. remove display objects, remove touch listeners, save state, etc
    composer.removeScene( true )
end

---------------------------------------------------------------------------------

-- Listener setup
scene:addEventListener( "create", scene )
scene:addEventListener( "show", scene )
scene:addEventListener( "hide", scene )
scene:addEventListener( "destroy", scene )

---------------------------------------------------------------------------------

return scene
