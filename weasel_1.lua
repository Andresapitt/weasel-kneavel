--
-- created with TexturePacker (http://www.codeandweb.com/texturepacker)
--
-- $TexturePacker:SmartUpdate:dff489e8caf352b6e35757d723f89180:20fc7c0205702b177c66599f6d46e58e:335e5fe926f8a095c0ee211febf64813$
--
-- local sheetInfo = require("mysheet")
-- local myImageSheet = graphics.newImageSheet( "mysheet.png", sheetInfo:getSheet() )
-- local sprite = display.newSprite( myImageSheet , {frames={sheetInfo:getFrameIndex("sprite")}} )
--

local SheetInfo = {}

SheetInfo.sheet =
{
    frames = {
    
        {
            -- Weasel_K_blink
            x=322,
            y=2,
            width=158,
            height=100,

            sourceX = 1,
            sourceY = 4,
            sourceWidth = 160,
            sourceHeight = 104
        },
        {
            -- Weasel_K_drive0001
            x=162,
            y=2,
            width=158,
            height=100,

            sourceX = 1,
            sourceY = 4,
            sourceWidth = 160,
            sourceHeight = 104
        },
        {
            -- Weasel_K_drive0002
            x=2,
            y=2,
            width=158,
            height=100,

            sourceX = 1,
            sourceY = 4,
            sourceWidth = 160,
            sourceHeight = 104
        },
    },
    
    sheetContentWidth = 482,
    sheetContentHeight = 104
}

SheetInfo.frameIndex =
{

    ["blink"] = 1,
    ["drive1"] = 2,
    ["drive2"] = 3,
}

function SheetInfo:getSheet()
    return self.sheet;
end

function SheetInfo:getFrameIndex(name)
    return self.frameIndex[name];
end

SheetInfo.sequenceData = {
    {
    name="driving",
    frames={2,3,2,3,2,3,2,1,2,3,3,2,3,2,1},
    time=1000,
    loopCount = 999,
    loopDirection = "forward"
    },
    {
    name="blink",
    frames={1,2},
    time=2000,
    loopCount = 100,
    },
    {
    name="all",
    frames={1,2,3},
    time=50,
    loopCount = 100,
    }

}

function SheetInfo:getSequenceData()
    return self.sequenceData;
end

return SheetInfo
