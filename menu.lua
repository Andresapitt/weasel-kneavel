---------------------------------------------------------------------------------
--
-- scene.lua
--
---------------------------------------------------------------------------------

local sceneName = ...

local composer = require( "composer" )

-- Load scene with same root filename as this file
local scene = composer.newScene( sceneName )

composer.recycleOnSceneChange = true


local _W = display.contentWidth
local _H = display.contentHeight
local xMin = display.screenOriginX
local yMin = display.screenOriginY
local xMax = display.contentWidth - xMin
local yMax = display.contentHeight - yMin
local xWidth = xMax-xMin;
local yHeight = yMax-yMin
local padding = 10

local options =
{
    effect = "fade",
    time = 500,
    params = {
        sampleVar1 = "my sample variable",
        sampleVar2 = "another sample variable"
    }
}




---------------------------------------------------------------------------------

local startGame = function( event )
         composer.gotoScene( "scene1", options )
end

function scene:create( event )
    local sceneGroup = self.view

     
end

function scene:show( event )
    local sceneGroup = self.view
    local phase = event.phase

    if phase == "will" then
        -- Called when the scene is still off screen and is about to move on screen

    elseif phase == "did" then
        -- Called when the scene is now on screen

    local menubg = display.newImageRect("images/01_start_menu-(1).png", 1136, 640)
    menubg.anchorX=0
    menubg.x =0; menubg.y = _H*.5
    menubg:addEventListener("tap", startGame)

    end 
end


     
function scene:hide( event )
    local sceneGroup = self.view
    local phase = event.phase

    if event.phase == "will" then
        -- Called when the scene is on screen and is about to move off screen
        --
        -- INSERT code here to pause the scene
        -- e.g. stop timers, stop animation, unload sounds, etc.)
    elseif phase == "did" then
        -- Called when the scene is now off screen

    end 
end


function scene:destroy( event )
    local sceneGroup = self.view



    -- Called prior to the removal of scene's "view" (sceneGroup)
    -- 
    -- INSERT code here to cleanup the scene
    -- e.g. remove display objects, remove touch listeners, save state, etc
end

---------------------------------------------------------------------------------

-- Listener setup
scene:addEventListener( "create", scene )
scene:addEventListener( "show", scene )
scene:addEventListener( "hide", scene )
scene:addEventListener( "destroy", scene )

---------------------------------------------------------------------------------

return scene
